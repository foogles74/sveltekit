import { supabase } from "$lib/supabaseClient";
import { fail, redirect } from '@sveltejs/kit';
import bcrypt from 'bcrypt'

/**@type {import('./$types').Actions} */
export const actions = {
    register: async ({ locals, cookies, url, request }) => {
        //Получим данные формы
        const formdata = await request.formData()
        let valid = formdata.get('valid')
        let login = formdata.get("login")
        let username = formdata.get("username")
        let password = formdata.get('password')
        console.log(password)
        if (valid == "false")
        {   
            return fail(409, { validfail: true }) 
        }
        if (Number(password.length) < 8)
		{
            return fail(409, { validfail: true }) 
		}
		if(!(/\d/.test(String(password)) &&  /[a-z]/.test(String(password)) &&  /[A-Z]/.test(String(password))))
		{
            return fail(409, { validfail: true }) 
		}
        //получим ID роли USER
        let { data, error } = await supabase
            .from('Roles')
            .select('id')
            .eq('name', 'USER')

        let user_role_id = data[0].id


        //Проверим, нет ли уже такого польз-ля
        let user = await supabase
            .from('Users')
            .select('username')
            .eq('username', username)

        let logins = await supabase
            .from('Users')
            .select('name')
            .eq('name', login)

        if (logins.data?.length) {//такой польз-ль уже есть
            return fail(409, { user: true })
        }

        //Создадим польз-ля с ролью USER
        const new_user = await supabase
            .from('Users')
            .insert(
                {
                    username: username,
                    name: login,
                    passwordHash: await bcrypt.hash(password, 10),
                    userAuthToken: crypto.randomUUID(),
                    roles: user_role_id
                }
            )
            console.log(new_user);
        throw redirect(303, '/')

    }

}