import { posts } from '$lib/data.js'

/** @type {import('./$types').LayoutServerLoad} */
export function load() {
    return {
        summaries: posts.map(post => ({
            slug: post.slug,
            title: post.title,
            some: 333
        }))
    }
}
