import { supabase } from "$lib/supabaseClient";
import { fail, redirect } from '@sveltejs/kit';
import bcrypt from 'bcrypt'

/**@type {import('./$types').Actions} */
export const actions = {
    login: async ({ locals, cookies, url, request }) => {
        //Получим данные формы
        const formdata = await request.formData()
        let login = formdata.get("login")
        let password = formdata.get('password')

        //Проверим, есть ли польз-ль
        let user = await supabase
            .from('Users')
            .select()
            .eq('name', login)

        let user_email = await supabase
            .from('Users')
            .select()
            .eq('username', login)
            
        if (!user.data.length) {//Такого польз-ля нет
            if (!user_email.data.length)
            {
                return fail(400, { is_user: false })   
            }
            else
            {
                user.data = user_email.data
            }    
        }          

      



        //проверим пароль
        const checkPassword = await bcrypt.compare(password, user.data[0].passwordHash)

        if (!checkPassword) {//пароль неправильный
            return fail(400, { bad_pwd: true })
        }

        //обновим данные польз-ля
        let token = crypto.randomUUID()
        const upd_user = await supabase
            .from('Users')
            .update({ userAuthToken: token, updated_at: new Date() })
            .eq('id', user.data[0].id)

        if (!upd_user.error) {//нет ошибок при update
            await locals.session.set({ user: { id: user.data[0].id, name: user.data[0].username, email : user.data[0].email, token: token }, views: 0, data1: [], data2: [] });

            throw redirect(303, '/')
        } else {//update вызвал ошибку

        }
    }


}