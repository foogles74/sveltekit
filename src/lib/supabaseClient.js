import { createClient } from '@supabase/supabase-js';
import { SECRET_API_KEY,SECRET_API_URL } from '$env/static/private'

export const supabase = createClient(SECRET_API_URL, SECRET_API_KEY)