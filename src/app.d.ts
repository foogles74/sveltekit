// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
interface Person {
	id: string,
	name: string,
	email: string
}

interface SessionData {
	views: number,
	data1: number[],
	data2: string[],
	user: Person
}

declare global {
	namespace App {
		interface Locals {
			session: import('svelte-kit-cookie-session').Session<SessionData>;
			stop: boolean;
		}

		interface Session extends SessionData { }
		// interface Error {}
		// interface Locals {}
		interface PageData { session: SessionData; }
		// interface Platform {}
	}
}
declare module '@fortawesome/pro-solid-svg-icons/index.es' {
	export * from '@fortawesome/pro-solid-svg-icons';
  }

export { };
